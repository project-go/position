# Проверка статусов страницы

- проверяет статусы страниц, мета тегов, h1, h2, хлебных крошек

## [Описание файлов](src/README.md)

## Запуск
- список запущенных сервисов
```sh
ps axu | grep go-daemon
```
- при `"service": true` программа стартует как сервис
    - в `log` пишется весь вывод с программы
    - в `meta-product-pid` pid запущенной программы
- добавление в крон (обеспечивает простой автозапуск, в случает если процесс погибнет или будет перезапуск)
```sh
*/10 * * * *	/var/www/***/data/***/goland/position.go/src/cron-meta.sh
```
- возможен запуск в двух конфигурациях
    - сначало `cd /var/www/***/data/***/goland/position.go/src`
    - `go run meta/main.go` - запуск на продакшен базе (как сервис в фоне)
    - `go run meta/main.go develop` - запуск на дев базе (как программа)

## Настройки
- нагрузка в час = `limit` / `sleep` * 3600
- `service` - запуск в качестве сервиса в фоне, или как программу
- `selectTimeout` - время в часах, через которое проверяются страницы
- `limit` - количество парарельных процессов
- `sleep` - пауза между запросами
- `errorTimeout` - время в часах, через коротое проверится страница, при возникновении ошибки
- `errorDomenTimeout` - время в минутах, через которое проверится страница, на домене с ошибкой
- `httpTimeout` - максимальное время в секундах, на чтения страницы
- файлы настроек для прода `meta/config/config.product.json` и дев сервера `meta/config/config.develop.json`
- пример файла `meta/config/config.origin.json`:
```json
{
    "service": true,
    "meta": {
        "selectTimeout": 5,
        "limit": 2,
        "sleep": 10,
        "errorTimeout": 30,
        "errorDomenTimeout": 5,
        "httpTimeout": 30
    },
    "redis": "redis://user:@localhost:6379/0",
    "db": "user:password@unix(/var/lib/mysql/mysql.sock)/table"
}
```

## Требования
- mysql
- redis

## Установка
- установка go
```sh
wget https://dl.google.com/go/go1.10.1.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.10.1.linux-amd64.tar.gz
```
- настройка окружения пользователя
```sh
echo 'PATH=$PATH:$HOME/bin:$HOME/go/bin:/usr/local/go/bin' >> ~/.bashrc
echo 'export PATH' >> ~/.bashrc
```
- установка внешних пакетов
```sh
go get -u github.com/go-sql-driver/mysql
go get -u github.com/garyburd/redigo/redis
go get -u golang.org/x/text/transform
go get -u golang.org/x/text/encoding/charmap
go get -u github.com/sevlyar/go-daemon
```
- ставим Redis
    - yum install redis
    - systemctl enable redis.service
    - systemctl start redis
