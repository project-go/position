#!/bin/bash

# chmod 0744 cron-meta.sh

# устаналиваем пути к исходникам и пакетам
PATH=$PATH:$HOME/bin:$HOME/go/bin:/usr/local/go/bin
export PATH

# полный путь до скрипта
ABSOLUTE_FILENAME=`readlink -e "$0"`
# каталог в котором лежит скрипт
DIRECTORY=`dirname "$ABSOLUTE_FILENAME"`

cd $DIRECTORY
go run meta/main.go  > /dev/null 2>&1
