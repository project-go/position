package page

import (
	// "fmt"
	"errors"
	// "os"
	"net/http"
	"io/ioutil"
	"strings"
	"golang.org/x/text/transform"
	"golang.org/x/text/encoding/charmap"
	"../application"
)

func getPage(url string) (src string, statusCode int16, redirect string, err error) {
	app := application.GetInstance()
	tr := &http.Transport{
		MaxIdleConns:       10,
		IdleConnTimeout:    app.Meta.HttpTimeout,
		// DisableCompression: true,
	}
	client := &http.Client{Transport: tr}
	client.CheckRedirect = func(req *http.Request, via []*http.Request) error {
        return errors.New("Redirect")
    }
	resp, err := client.Get(url)
	defer resp.Body.Close()
	statusCode = int16(resp.StatusCode)
	if(err!=nil) {
		if resp.StatusCode == http.StatusFound {
			err = nil
			url, _ := resp.Location()
			redirect = url.String()
		}
		return
	}

	isUtf8 := strings.Contains(strings.ToLower(resp.Header.Get("Content-Type")), "charset=utf-8")
	if(isUtf8) {
		page, _ := ioutil.ReadAll(resp.Body)
		src = string(page)
	} else {
		body := transform.NewReader(resp.Body, charmap.Windows1251.NewDecoder())
		page, _ := ioutil.ReadAll(body)
		src = string(page)
	}
	return
}

func getResponse(item Saproc) (response response, err error) {
	src, page_status, redirect, err := getPage(item.Url)
	// fmt.Printf("metaGetStatus1: %+v\n", src)
	// fmt.Printf("metaGetStatus2: %+v\n", page_status)
	// fmt.Printf("metaGetStatus3: %+v\n", redirect)
	// fmt.Printf("metaGetStatus4: %+v\n", err)
	if(err!=nil) {
		return
	}
	response = parsePage(src, item)
	response.page_status = page_status
	if(redirect!="") {
		response.title = redirect
	}
	// fmt.Printf("metaGetStatus: %+v\n", response)
	// fmt.Printf("metaGetStatus: %+v\n", src)
	// os.Exit(0)
	return
}
