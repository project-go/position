package page

import (
	"fmt"
	"time"
	"log"
	"../application"
)

func Proverka(item Saproc) {
	app := application.GetInstance()
	defer app.Wg.Done()
	start := time.Now()

	// item.Url = "http://develop.dev-tm.ru/goland/cp1251.php"
	// item.Url = "http://develop.dev545-tm.ru/goland/utf-8.php"
	// item.Url = "http://develop.dev-tm.ru/goland/redirect.php"
	domen := getDomen(item.Url)
	app.Mu.Lock()
	if(app.Domen.IsError(domen)) {
		fmt.Printf("Timeout domen: %+v \n", domen)
		setErrorUrl(item.Id, app.Meta.ErrorDomenTimeout)
		app.Mu.Unlock()
		return
	}
	app.Mu.Unlock()

	response, err := getResponse(item)

	if(app.Service) {
		log.Printf("response id:%+v status:%+v url:%+v\n", response.id, response.page_status, item.Url)
	} else {
		fmt.Printf("%+v \n", response)
	}
	// fmt.Printf("%+v \n", err)
	if(err!=nil) {
		fmt.Printf("Error: %+v \n", domen)
		app.Mu.Lock()
		setErrorUrl(item.Id, app.Meta.ErrorDomenTimeout)
		app.Domen.SetError(domen)
		app.Mu.Unlock()
		return
	}

	update(response);
	if(!app.Service) {
		end := time.Now()
		fmt.Printf("%f s\n", end.Sub(start).Seconds())
	}
}
