package page

import (
	"time"
	"../application"
)

func update(response response) {
	app := application.GetInstance()
	app.Mu.Lock()
	app.Db.Exec(
		"UPDATE meta SET `updateError`='0', `page_status` = ?, `title_item` = ?, `description_item` = ?, `keywords_item` = ?, `h1_item` = ?, `h2_item` = ?, `breadcrumbs_status` = ?, `update` = ?, `date`=NOW() WHERE id = ?",
		response.page_status,
		response.title,
		response.description,
		response.keywords,
		response.h1,
		response.h2,
		response.breadcrumbs_status,
		time.Now().Unix(),
		response.id,
	)
	app.Mu.Unlock()
	// app.PanicOnErr(err);
}
