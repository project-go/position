package page

import (
	"regexp"
	"strings"
)

func getDomen(url string) (domen string) {
	var search = regexp.MustCompile(`\/\/([^\/]+)\/`).FindStringSubmatch(strings.ToLower(url))
	if(len(search)==2) {
		domen = search[1]
	}
	return
}
