package page

import (
	"time"
	"../application"
)

func setErrorUrl(id int32, timeout time.Duration) {
	app := application.GetInstance()
	app.Db.Exec(
		"UPDATE meta SET `updateError` = ? WHERE id = ?",
		time.Now().Add(timeout).Unix(),
		id,
	)
}
