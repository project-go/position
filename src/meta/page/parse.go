package page

import (
	// "fmt"
	"html"
	"regexp"
	"strings"
)

func unescapeStringTrim(str string) (string) {
	return html.UnescapeString(html.UnescapeString(strings.TrimSpace(str)));
}

func unescapeString(str string) (string) {
	return html.UnescapeString(html.UnescapeString(str));
}

func parseTag(reg string, src string) (tag string) {
	search := regexp.MustCompile(reg).FindStringSubmatch(src)
	if(len(search)==2) {
		tag = unescapeStringTrim(search[1])
	}
	return
}

func parseTagAll(reg string, src string) (tag string) {
	searchAll := regexp.MustCompile(reg).FindAllStringSubmatch(src, -1)
	if(len(searchAll)>0) {
		tagList := make([]string, len(searchAll))
		for key, value := range searchAll {
			tagList[key] = strings.TrimSpace(value[1]);
		}
		tag = unescapeString(strings.Join(tagList, "\n"))
	}
	return
}

func parsePage(src string, item Saproc) (response response) {
	response.id = item.Id

	src = regexp.MustCompile(`\s\s+`).ReplaceAllString(src, " ")

	response.title = parseTag(`(?ims)<title[^>]*>([^>]*)</title>`, src)
	response.keywords = parseTag(`(?ims)<meta[^>]*name="keywords"[^>]*content="([^"]*)"[^>]*>`, src)
	response.description = parseTag(`(?ims)<meta[^>]*name="description"[^>]*content="([^"]*)"[^>]*>`, src)

	response.h1 = parseTagAll(`(?imsU)<h1[^>]*>(.*)</h1>`, src)
	response.h2 = parseTagAll(`(?imsU)<h2[^>]*>(.*)</h2>`, src)

	if(len(item.Breadcrumbs)==0) {
		return
	}
	src = regexp.MustCompile(`(?imsU)<script[^>]*>.*</script>`).ReplaceAllString(src, " ")
	src = regexp.MustCompile(`(?imsU)<head[^>]*>.*</head>`).ReplaceAllString(src, " ")
	src = regexp.MustCompile(`(?imsU)</?[a-z][^>]*>`).ReplaceAllString(src, " ")
	src = regexp.MustCompile(`(?imsU)<!--.*-->`).ReplaceAllString(src, " ")
	src = regexp.MustCompile(`\s\s+`).ReplaceAllString(src, " ")
	src = strings.ToLower(src)

	isBreadcrumbs := strings.Contains(src, strings.ToLower(item.Breadcrumbs))
	if(isBreadcrumbs) {
		response.breadcrumbs_status = 1
		return
	}
	item.Breadcrumbs = strings.Replace(item.Breadcrumbs, "/", "", -1)
	item.Breadcrumbs = regexp.MustCompile(`\s\s+`).ReplaceAllString(item.Breadcrumbs, " ")
	isBreadcrumbs = strings.Contains(src, strings.ToLower(item.Breadcrumbs))
	if(isBreadcrumbs) {
		response.breadcrumbs_status = 1
	}
	return
}
