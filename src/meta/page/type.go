package page

type Saproc struct {
	Id              int32
	Url             string
	Breadcrumbs     string
}

type response struct {
	id                  int32
	page_status         int16
	title               string
	description         string
	keywords            string
	h1                  string
	h2                  string
	breadcrumbs_status  int8
}
