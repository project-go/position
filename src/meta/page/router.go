package page

import (
	"fmt"
	"time"
	 "../application"
)

func Router() {
	app := application.GetInstance()
	for {
		if(!app.Service) {
			fmt.Println("start")
		}
		rows, err := app.Db.Query(
			"SELECT `id`, `url`, `breadcrumbs` FROM meta WHERE `deleted`=0 AND `update`<? AND `updateError`<? ORDER BY `update`, `updateError` LIMIT 0, ?",
			time.Now().Add(-app.Meta.SelectTimeout).Unix(),
			time.Now().Unix(),
			app.Meta.Limit,
		)
		defer rows.Close()
		if(err==nil) {
			for rows.Next() {
				var item Saproc
				err = rows.Scan(&item.Id, &item.Url, &item.Breadcrumbs)
				if(!app.Service) {
					fmt.Printf("item: %+v\n", item)
				}
				// app.PanicOnErr(err)
				app.Wg.Add(1)
				go Proverka(item)
			}
			app.Wg.Wait()
		}
		time.Sleep(app.Meta.Sleep)
		app.Reload.IsWork()
	}
}
