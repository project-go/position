package application

import (
	"fmt"
	"os"
	// "time"
	"encoding/json"
	"sync"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

// Тип singleton, экземпляр которого требуется создавать
type singleton struct {
	Mode string
	Service bool
	Reload Reload
	Meta MetaConfig
	Db *sql.DB
	Domen Domen
	Wg sync.WaitGroup
	Mu sync.Mutex
}

func (s *singleton) PanicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}

func (s *singleton) PanicOnErrString(err error, str string) {
	if err != nil {
		println(str)
		panic(err)
	}
}

func (s *singleton) setDb(conf string) {
	var err error
	s.Db, err = sql.Open("mysql", conf)
	s.PanicOnErr(err)
	s.Db.SetMaxOpenConns(10)
}

func (s *singleton) Close() {
	s.Db.Close()
	s.Domen.Close()
}

// Инстанс, который будет содержать единственный экземпляр типа *singleton
var instance *singleton

// Объект, который позволяет выполнять некоторое действие только один раз
var once sync.Once

// Функция создает и возвращает экземпляр типа *singleton
func GetInstance() *singleton {
	once.Do(func() {
		instance = &singleton{}
		args := os.Args[1:]
		var mode string = "product"
		if(len(args)>0) {
			mode = args[0]
		}
		println("config start "+ mode)
		path, _ := os.Getwd()
		path = path + "/meta/config/config."+ mode +".json"
		println(path)

		file, _ := os.Open(path)
		decoder := json.NewDecoder(file)
		conf := new(appConfig)
		err := decoder.Decode(&conf)
		instance.PanicOnErrString(err, "Не прочитан файл конфигурации")

		instance.Mode = mode
		instance.Service = conf.Service
		instance.Reload.init()
		instance.Meta.set(conf.Meta);
		fmt.Printf("config: %+v\n", instance.Meta)
		instance.setDb(conf.Db);
		instance.Domen.init(conf.Redis);
	})
	return instance
}
