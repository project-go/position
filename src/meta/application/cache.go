package application

import (
	// "fmt"
	// "time"
	"github.com/garyburd/redigo/redis"
)

type Cache struct {
	conn redis.Conn
}

func (c *Cache) Close() {
	c.conn.Close()
}

func (c *Cache) init(config string) {
	var err error
	c.conn, err = redis.DialURL(config)
	instance.PanicOnErr(err)
	//
	// mkey := "record_21"
	// item, err := c.Get(mkey)
	// fmt.Printf("1 get %+v\n", item)
	//
	// c.Set(mkey, "45", 5)
	// time.Sleep(time.Microsecond)
	//
	// item, err = c.Get(mkey)
	// fmt.Printf("2 get %+v\n", item)
	//
	// time.Sleep(7 * time.Second)
	// item, err = c.Get(mkey)
	// fmt.Printf("3 get %+v\n", item)
}

func (c *Cache) Set(mkey string, value string, ttl int) {
	// добавляет запись, https://redis.io/commands/set
	result, err := redis.String(c.conn.Do("SET", mkey, value, "EX", ttl))
	instance.PanicOnErr(err)
	if result != "OK" {
		// panic("result not ok: " + result)
	}
}

func (c *Cache) Get(mkey string) (string, error) {
	// println("get", mkey)
	// получает запись, https://redis.io/commands/get
	data, err := c.conn.Do("GET", mkey)
	item, err := redis.String(data, err)
	// если записи нету, то для этого есть специальная ошибка, её надо обрабатывать отдеьно, это почти штатная ситуация, а не что-то страшное
	if err == redis.ErrNil {
		// fmt.Println("Record not found in redis (return value is nil)")
		return "", redis.ErrNil
	} else if err != nil {
		return "", err
		instance.PanicOnErr(err)
	}
	return item, nil
}
