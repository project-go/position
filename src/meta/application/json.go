package application

type appConfig struct {
	Service bool
	Meta meta
	Redis string
	Db string
}
