package application

import (
	"time"
	"os"
)

type Reload struct {
	Date string
}

func metka() (string) {
	 return time.Now().Format("2006.01/02")
}

func (r *Reload) init() {
	r.Date = metka()
}

func (r *Reload) IsWork() {
	if(r.Date!=metka()) {
		println("exit work")
		os.Exit(0);
	}
}
