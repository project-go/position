package application

import (
	"time"
)

type meta struct {
	SelectTimeout int
	Limit int
	Sleep int
	ErrorTimeout int
	ErrorDomenTimeout int
	HttpTimeout int
}

type MetaConfig struct {
	SelectTimeout time.Duration
	Limit int
	Sleep time.Duration
	ErrorTimeout time.Duration
	ErrorDomenTimeout time.Duration
	ErrorDomenTtl int
	HttpTimeout time.Duration
}

func (m *MetaConfig) set(c meta) {
	m.SelectTimeout = time.Hour * time.Duration(c.SelectTimeout)
	m.Limit = c.Limit
	m.Sleep = time.Second * time.Duration(c.Sleep)
	m.ErrorTimeout = time.Minute * time.Duration(c.ErrorTimeout)
	m.ErrorDomenTimeout = time.Minute * time.Duration(c.ErrorDomenTimeout)
	m.ErrorDomenTtl = c.ErrorDomenTimeout * 60
	m.HttpTimeout = time.Second * time.Duration(c.HttpTimeout)
}
