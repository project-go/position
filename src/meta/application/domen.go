package application

const isOn = "Domen error"

type Domen struct {
	Cache
}

func (d *Domen) SetError(mkey string) {
	 d.Set(mkey, isOn, instance.Meta.ErrorDomenTtl)
}

func (d *Domen) IsError(key string) (bool) {
	if(key=="") {
		return false
	}
	value, _ := d.Get(key)
	return value==isOn
}
