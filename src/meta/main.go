package main

import (
	// "fmt"
	// "path/filepath"
	"os"
	"time"
	"log"
	"github.com/sevlyar/go-daemon"
	 "./application"
	 "./page"
)

func main() {
	app := application.GetInstance()
	defer app.Close()

	if(app.Service) {
		os.MkdirAll("log/" + time.Now().Format("2006.01"), 0777)
		cntxt := &daemon.Context{
			PidFileName: "meta-"+ app.Mode +"-pid",
			PidFilePerm: 0644,
			LogFileName: "log/" + time.Now().Format("2006.01/02.meta.log"),
			LogFilePerm: 0640,
			WorkDir:     "./",
			Umask:       027,
			Args:        []string{"[go-daemon position.meta."+ app.Mode +"]"},
		}

		d, err := cntxt.Reborn()
		if err != nil {
			log.Fatal("Unable to run: ", err)
		}
		if d != nil {
			return
		}
		defer cntxt.Release()

		log.Print("- - - - - - - - - - - - - - -")
		log.Print("daemon started")
	}
	// return
	page.Router()
}
