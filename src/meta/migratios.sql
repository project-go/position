
DROP TABLE IF EXISTS `meta_history`;
DROP TABLE IF EXISTS `meta`;
CREATE TABLE `meta` (
  `id` int(32) NOT NULL,
  `project_id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `update` int(11) DEFAULT '0',
  `updateError` int(11) DEFAULT '0',
  `deleted` int(11) DEFAULT '0',
  `url` varchar(250) NOT NULL,
  `page_status` int(8) DEFAULT '0',
  `title` text,
  `title_item` text,
  `title_status` enum('1','0') DEFAULT '0',
  `description` text,
  `description_item` text,
  `description_status` enum('1','0') DEFAULT '0',
  `keywords` text,
  `keywords_item` text,
  `keywords_status` enum('1','0') DEFAULT '0',
  `h1` text,
  `h1_item` text,
  `h1_status` enum('1','0') DEFAULT '0',
  `h2` text,
  `h2_item` text,
  `h2_status` enum('1','0') DEFAULT '0',
  `breadcrumbs` text,
  `breadcrumbs_item` text,
  `breadcrumbs_status` enum('1','0') DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELIMITER $$
CREATE TRIGGER `history` AFTER UPDATE ON `meta` FOR EACH ROW BEGIN
IF NEW.page_status!=OLD.page_status
    OR NEW.title_status!=OLD.title_status
    OR NEW.description_status!=OLD.description_status
    OR NEW.keywords_status!=OLD.keywords_status
    OR NEW.h1_status!=OLD.h1_status
    OR NEW.h2_status!=OLD.h2_status
    OR NEW.breadcrumbs_status!=OLD.breadcrumbs_status THEN
INSERT INTO meta_history SET
    meta_id = NEW.id,
    project_id = NEW.project_id,
    url = NEW.url,
    page_status = NEW.page_status,
    title = NEW.title_item,
    title_status = NEW.title_status,
    description = NEW.description_item,
    description_status = NEW.description_status,
    keywords = NEW.keywords_item,
    keywords_status = NEW.keywords_status,
    h1 = NEW.h1_item,
    h1_status = NEW.h1_status,
    h2 = NEW.h2_item,
    h2_status = NEW.h2_status,
    breadcrumbs = NEW.breadcrumbs,
    breadcrumbs_status = NEW.breadcrumbs_status;
END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `status` BEFORE UPDATE ON `meta` FOR EACH ROW BEGIN
SET NEW.title_status = (LOWER(NEW.title) = LOWER(NEW.title_item));
SET NEW.description_status = (LOWER(NEW.description) = LOWER(NEW.description_item));
SET NEW.keywords_status = (LOWER(NEW.keywords) = LOWER(NEW.keywords_item));
SET NEW.h1_status = (LOWER(NEW.h1) = LOWER(NEW.h1_item));
SET NEW.h2_status = (LOWER(NEW.h2) = LOWER(NEW.h2_item));
IF NEW.update=OLD.update AND (NEW.title!=OLD.title
    OR NEW.description!=OLD.description
    OR NEW.keywords!=OLD.keywords
    OR NEW.h1!=OLD.h1
    OR NEW.h2!=OLD.h2
    OR NEW.breadcrumbs!=OLD.breadcrumbs) THEN
SET NEW.`update` = 0;
END IF;
END
$$
DELIMITER ;

ALTER TABLE `meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `deleted` (`deleted`,`update`,`updateError`);

ALTER TABLE `meta`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

ALTER TABLE `meta`
  ADD CONSTRAINT `meta_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;


DROP TABLE IF EXISTS `meta_history`;
CREATE TABLE `meta_history` (
  `id` int(32) NOT NULL,
  `meta_id` int(32) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `project_id` int(11) NOT NULL,
  `url` varchar(250) NOT NULL,
  `page_status` int(8) DEFAULT '0',
  `title` text,
  `title_status` enum('1','0') DEFAULT '0',
  `description` text,
  `description_status` enum('1','0') DEFAULT '0',
  `keywords` text,
  `keywords_status` enum('1','0') DEFAULT '0',
  `h1` text,
  `h1_status` enum('1','0') DEFAULT '0',
  `h2` text,
  `h2_status` enum('1','0') DEFAULT '0',
  `breadcrumbs` text,
  `breadcrumbs_status` enum('1','0') DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `meta_history`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `meta_id_2` (`meta_id`,`url`,`date`),
  ADD KEY `meta_id` (`meta_id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `project_id_2` (`project_id`,`meta_id`),
  ADD KEY `url` (`url`);

ALTER TABLE `meta_history`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;

ALTER TABLE `meta_history`
  ADD CONSTRAINT `meta_history_meta_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;
