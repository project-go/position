package agent

import (
	"os"
	"time"
	"log"
	"github.com/sevlyar/go-daemon"
)

func Start(mode string) (bool) {
	os.MkdirAll("log/" + time.Now().Format("2006.01"), 0777)
	cntxt := &daemon.Context{
		PidFileName: "meta-"+ mode +"-pid",
		PidFilePerm: 0644,
		LogFileName: "log/" + time.Now().Format("2006.01/02.meta.log"),
		LogFilePerm: 0640,
		WorkDir:     "./",
		Umask:       027,
		Args:        []string{"[go-daemon position.meta."+ mode +"]"},
	}

	d, err := cntxt.Reborn()
	if err != nil {
		log.Fatal("Unable to run: ", err)
	}
	if d != nil {
		log.Print("Unable d != nil")
		return true
	}
	defer cntxt.Release()

	log.Print("- - - - - - - - - - - - - - -")
	log.Print("daemon started")
	return false
}
